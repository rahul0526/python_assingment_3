# Write a program to search given element from the list. Use your own function to search an element from list.
# Note: Function should receive variable length arguments and search each of these arguments present in the list.


def search(element):
    mylist = [5, 3, 7, 3, 0, 9, 4]
    mylist.sort()
    first = 0
    last = len(mylist) - 1
    found = False

    while first <= last and not found:
        mid = int((first + last) / 2)
        if mylist[mid] == element:
            found = True
        else:
            if element < mylist[mid]:
                last = mid - 1
            else:
                first = mid + 1
    if found:
        print(f'Element {element} is present in the list')
    else:
        print(f'Element {element} is not present in the list')


to_search = int(input('Enter the number to search : '))
search(to_search)
