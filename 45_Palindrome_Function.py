# Write a program to check given string is Palindrome or not.
# (Use function Concepts and Required keyword, Default parameter concepts)


def ispalindrome(tocheck):
    rev_string = tocheck[::-1]
    if tocheck == rev_string:
        print(f'{tocheck} is palindrome')
    else:
        print(f'{tocheck} is  not palindrome')


inputstr = input('Enter a string = ')
ispalindrome(inputstr)
