# Open existing text file and reverse its contents. i.e

file_open = open('Sample.txt', 'r')
# a) print the last line as first line and first line as last line (Reverse the lines of the file)


split_data = file_open.read().split('\n')
first_line = split_data[-1]
last_line = split_data[0]
split_data.pop()
split_data.pop(0)
split_data.insert(0, first_line)
split_data.append(last_line)
for line in split_data:
    print(line)
file_open.close()
# b) print characters of file from last character of file till the first character of the file.
# (Reverse entire contents of  file )


file_open = open('Sample.txt', 'r')
split_data = file_open.read().split('\n')
for line in split_data[::-1]:
    print(line)
file_open.close()
