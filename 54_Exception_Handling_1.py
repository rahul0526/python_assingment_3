# Write a program to handle the following exceptions in you program.
# KeyboardInterrupt
import time
try:
    while True:
        print("Don't press ctrl+C")
        time.sleep(1)
except KeyboardInterrupt:
    print("KeyboardInterrupt :  User interruption detected")

# NameError
try :
    ip = 'Name'
    print(IP)
except NameError:
    print('NameError : Correction in variable name.')

# ArithmeticError Note: Make use of try, except, else: block statements.
a = 60
b = 0
try:
    c = a/b
    print(c)
except ArithmeticError:
    print('Arithmetic error')
else:
    print('no error')