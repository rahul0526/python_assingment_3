# In a given directory search all text files for the pattern "Treasure".
# a) Find how many text files has the pattern.
# b) Count how many times pattern repeats in each file
import glob
total_count = 0
for file in glob.glob("*.txt"):
    pattern_count = 0
    text_file = open(file, 'r')
    split_text = text_file.read().split()
    for to_search in split_text:
        if to_search == 'Treasure':
            pattern_count += 1
    if pattern_count >= 1:
        print(f'{file} has word "Treasure" with total count = {pattern_count}')
        total_count += 1
    else:
        print(f"{file} doesn't contain the word 'Treasure' ")
print(f'{total_count} files contains the word "Treasure".')
