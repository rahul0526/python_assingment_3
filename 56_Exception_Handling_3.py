# Write a program to handle following exceptions using Try block.
# IO Error while you try writing contents into the file that is opened in read mode only.
try:
    open_file = open('Sample.txt', 'r')
    open_file.write('Good morning !')
except IOError:
    print('IOError')

# ValueError
try:
    a = int('one')
    print(a)
except ValueError:
    print('ValueError')
