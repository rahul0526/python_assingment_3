# Write a program to generate a Fibonacci series using a function called fib(n),
# Where ‘n’ is user specified argument specifies number of elements in the series.


def fib(n):
    first_number = 0
    second_number = 1
    count = 1
    if n == 0:
        print('Please enter positive integer')
    elif n == 1:
        print(first_number)
    else:
        while count <= n:
            third_number = first_number + second_number
            print(first_number)
            first_number = second_number
            second_number = third_number
            count += 1


# Fibonacci Series upto Nth Element
N = int(input('Enter Number of Elements : '))
fib(N)

