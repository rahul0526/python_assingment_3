
# a) Read 10 character at a time and then print its current position of file object. Repeat this operation till the EOF.


file_open = open("Sample.txt", "r")
while True:
    read_10_char = file_open.read(10)
    if not read_10_char:
        break
    else:
        print(read_10_char)
        print(file_open.tell())
file_open.close()

# b) Reset the file pointer after reading 100 Character from file (Use Seek function to reset)


file_open = open("Sample.txt", "r")
while file_open.tell() < 100:
    read_10_char = file_open.read(10)
    if not read_10_char:
        break
    else:
        print(read_10_char)
        print(file_open.tell())
file_open.seek(0)
print(file_open.read(10))
file_open.close()

# c) Open the file in read mode and start printing the contents from 5th line onwards.


file_open = open("Sample.txt", "r")
lineno = 0
for line in file_open:
    if lineno < 5:
        lineno += 1
        continue
    else:
        print(line)
file_open.close()
