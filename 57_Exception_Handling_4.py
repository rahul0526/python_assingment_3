# Try implementing atleast any 5 exceptions in you program
# Index Error
try:
    lis = [0,1]
    print(lis[2])
except IndexError:
    print('Index Error')

# Key Error
try:
    dictionary = {1:'one', 2:'two', 3:'three'}
    print(dictionary[4])
except KeyError:
    print('KeyError')

# Import Error
try:
    import xlrd
except ImportError:
    print('Import Error')

# FileNotFound Error
try:
    file_open = open('text.txt')
except FileNotFoundError:
    print('File Not Found')

# Stop Iteration Error Error
try:
    def generator():
        i = 0
        yield i
        i += 1
        yield i
    call = generator()
    print(next(call))
    print(next(call))
    print(next(call))
except StopIteration:
    print('Stop Iteration Error')
