# a) Open the file in read mode and read all its contents on to STDOUT.
file_open = open("Sample.txt", "r")
print("Contents in the File -\n", file_open.read())
file_open.close()

# b) Open the file in write mode and enter 5 new lines of strings in to the new file.
file_open = open("Sample.txt", "w")
file_open.write('India is my country\nAll indians are my brothers and sister\nI love my country\n'
                'I am proud of its rich and varied heritage\nI shall always strive to be worthy of it')
file_open.close()
# c) Open file in Append mode and add 5 lines of text into it.
file_open = open("Sample.txt", "a")
file_open.write('\nI shall give my parents, teachers and all elders respect and treat everyone with courtesy\n'
                'To my country and my people\nI pledge my devotion\nIn their well-being and prosperity alone\n'
                'lies my happiness')
file_open.close()
