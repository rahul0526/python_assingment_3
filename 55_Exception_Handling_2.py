# Write a program for converting weight from Pound to Kilo grams.
# Use assertion for the negative weight.

try:
    pound = float(input('Enter weight in pounds :'))
    assert (pound >= 0)
    kilo = pound /2.205
    print(f'{pound} lbs in kilos = {kilo} kg')
except AssertionError:
    print('Assertion Error : Negative Weight')


# Use assertion to weight more than 100 KG
try:
    pound = float(input('Enter weight in pounds :'))
    kilo = pound /2.205
    assert (kilo <= 100)
    print(f'{pound} lbs in kilos = {kilo} kg')
except AssertionError:
    print('Assertion Error : Weight greater than 100 kg')
