# functions to sort numbers (Use loops for  sorting, do not use built in function)
def sort_num(num_list):
    for i in range(len(num_list)):
        for j in range(i, len(num_list)):
            if num_list[i] >= num_list[j]:
                k = num_list[i]
                num_list[i] = num_list[j]
                num_list[j] = k
                j += 1
                continue
    return num_list


# function to search given element through binary search method.(Refer to net for the Binary search algorithm)
def search(to_search, num_list):
    num_list.sort()
    first = 0
    last = len(num_list) - 1
    found = False

    while first <= last and not found:
        mid = int((first + last) / 2)
        if num_list[mid] == to_search:
            found = True
        else:
            if to_search < num_list[mid]:
                last = mid - 1
            else:
                first = mid + 1
    if found:
        return f'Element is at {mid} position in list'
    else:
        return 'Unsuccessful search'


# function to reverse the given string
def rev_str(ip_string):
    return ip_string[::-1]
