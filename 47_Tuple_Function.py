# Write function to extend the tuple with elements of list. Pass list and Tuple as parameter to the function.


def extend_tuple(ip_list, ip_tuple):
    tup = list(ip_tuple)
    lis = tup + ip_list
    print(lis)


tuple_in = (5, 3, 7, 3)
list_in = [9, 3, 8, 1]
extend_tuple(list_in, tuple_in)
