# Write a function to find the biggest of 4 numbers.

# a) All numbers are passed as arguments separately (Required argument)


def biggest_number_required(no1, no2, no3, no4):
    if no1 > no2 and no1 > no3 and no1 > no4:
        print(no1, "is the Biggest number")
    elif no2 > no1 and no2 > no3 and no2 > no4:
        print(no2, "is the Biggest number")
    elif no3 > no2 and no3 > no1 and no3 > no4:
        print(no3, "is the Biggest number")
    else:
        print(no4, "is the Biggest number")

# b) use default values for arguments (Default arguments)


def biggest_number_default(no1, no2, no3, no4=20):
    if no1 > no2 and no1 > no3 and no1 > no4:
        print(no1, "is the Biggest number")
    elif no2 > no1 and no2 > no3 and no2 > no4:
        print(no2, "is the Biggest number")
    elif no3 > no2 and no3 > no1 and no3 > no4:
        print(no3, "is the Biggest number")
    else:
        print(no4, "is the Biggest number")


biggest_number_default(10, 3, 5)
biggest_number_required(12, 67, 34, 45)
