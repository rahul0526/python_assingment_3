# Create a package of all programs you have done in earlier.
# a. All programs related to strings -
# Stringpackage.py


def sort_num(num_list):
    for i in range(len(num_list)):
        for j in range(i, len(num_list)):
            if num_list[i] >= num_list[j]:
                k = num_list[i]
                num_list[i] = num_list[j]
                num_list[j] = k
                j += 1
                continue
    return num_list


def bin_search(to_search, num_list):
    num_list.sort()
    first = 0
    last = len(num_list) - 1
    found = False

    while first <= last and not found:
        mid = int((first + last) / 2)
        if num_list[mid] == to_search:
            found = True
        else:
            if to_search < num_list[mid]:
                last = mid - 1
            else:
                first = mid + 1
    if found:
        return f'Element is at {mid} position in list'
    else:
        return 'Unsuccessful search'


def rev_str(ip_string):
    return ip_string[::-1]


def concat(string1, string2):
    return string1+string2


def count_vowel(given_string):
    lower_string = given_string.lower()
    countA = 0
    countE = 0
    countI = 0
    countO = 0
    countU = 0
    count = 0
    for index in range(0,len(lower_string)):
        if lower_string[index] == 'a':
            countA += 1
            count += 1
        elif lower_string[index] == 'e':
            countE += 1
            count += 1
        elif lower_string[index] == 'i':
            countI += 1
            count += 1
        elif lower_string[index] == 'o':
            countO += 1
            count += 1
        elif lower_string[index] == 'u':
            countU += 1
            count += 1
    return f'Total number of vowel = {count}, a = {countA}, e = {countE}, i = {countI}, o = {countO}, u = {countU}'


def substring(stri, delim):
    substrin = stri.split(delim)
    return substrin
# b. All programs related to Lists -
# ListPackage.py


def search_element_presence(list_name, to_search):
    if to_search in list_name:
        return 'Element is present'
    else:
        return 'Element is not present'


def count_element(list_name, to_search):
    count = 0
    for index in list_name:
        if index == to_search:
            count += 1
    if count >= 1:
        return count
    else:
        return 'Element is not present'


def avg_list(list_name):
    sum_min = 0
    for i in list_name:
        sum_min += i
    avg = sum_min / len(list_name)
    return avg

# c. All programs related to Tuple -
# TuplePackage.py


def count_tup_ele(tup, element):
    return tup.count(element)


def comp_3_tup(tup1, tup2, tup3):
    if tup1 > tup2 and tup1 > tup3:
        return tup1
    elif tup2 > tup1 and tup2 > tup3:
        return tup2
    else:
        return tup3


def extend_tuple(ip_list, ip_tuple):
    tup = list(ip_tuple)
    lis = tup + ip_list
    print(lis)
# d. All programs related to Dictionary -
# DictionaryPackage.py


def print_dict_keys(dictionary):
    for i in dictionary.keys():
        print(i)


def print_dict_values(dictionary):
    for i in dictionary.values():
        print(i)


def update_dict(dictionary, key, value):
    dictionary.update({key, value})


def big_3_dict(dict1, dict2, dict3):
    l1 = list(dict1)
    l2 = list(dict2)
    l3 = list(dict3)
    if l1 > l2 and l1 > l3:
        return dict1
    elif l2 > l1 and l2 > l3:
        return dict2
    elif l3 > l2 and l3 > l1:
        return dict3

# e. All programs related to Functions -
# FunctionPackage.py


def fib(n):
    first_number = 0
    second_number = 1
    count = 1
    if n == 0:
        print('Please enter positive integer')
    elif n == 1:
        print(first_number)
    else:
        while count <= n:
            third_number = first_number + second_number
            print(first_number)
            first_number = second_number
            second_number = third_number
            count += 1


def search(element):
    mylist = [5, 3, 7, 3, 0, 9, 4]
    mylist.sort()
    first = 0
    last = len(mylist) - 1
    found = False

    while first <= last and not found:
        mid = int((first + last) / 2)
        if mylist[mid] == element:
            found = True
        else:
            if element < mylist[mid]:
                last = mid - 1
            else:
                first = mid + 1
    if found:
        return f'Element {element} is present in the list'
    else:
        return f'Element {element} is not present in the list'


def ispalindrome(tocheck):
    rev_string = tocheck[::-1]
    if tocheck == rev_string:
        return f'{tocheck} is palindrome'
    else:
        return f'{tocheck} is  not palindrome'


def biggest_number_required(no1, no2, no3, no4):
    if no1 > no2 and no1 > no3 and no1 > no4:
        return no1
    elif no2 > no1 and no2 > no3 and no2 > no4:
        return no2
    elif no3 > no2 and no3 > no1 and no3 > no4:
        return no3
    else:
        return no4


# f. All programs related to Files  -
# FilePackage.py


def read_file(file_name):
    file_open = open(file_name, "r")
    print("Contents in the File -\n", file_open.read())
    file_open.close()


def write_file(file_name, to_write):
    file_open = open(file_name, "w")
    file_open.write(to_write)
    file_open.close()


def append_to_file(file_name, to_append):
    file_open = open(file_name, "a")
    file_open.write(to_append)
    file_open.close()


def read_n_characters(file_name, no_of_characters_to_read):
    file_open = open(file_name, "r")
    while True:
        read_n_char = file_open.read(no_of_characters_to_read)
        if not read_n_char:
            break
        else:
            print(read_n_char)
            print(file_open.tell())
    file_open.close()


def reverse_file(file_name):
    file_open = open(file_name, 'r')
    split_data = file_open.read().split('\n')
    for line in split_data[::-1]:
        print(line)
    file_open.close()


def swap_first_n_last_line(file_name):
    file_open = open(file_name, 'r')
    split_data = file_open.read().split('\n')
    first_line = split_data[-1]
    last_line = split_data[0]
    split_data.pop()
    split_data.pop(0)
    split_data.insert(0, first_line)
    split_data.append(last_line)
    for line in split_data:
        print(line)
    file_open.close()


def search_pattern(pattern):
    import glob
    total_count = 0
    for file in glob.glob("*.txt"):
        pattern_count = 0
        text_file = open(file, 'r')
        split_text = text_file.read().split()
        for to_search in split_text:
            if to_search == pattern:
                pattern_count += 1
        if pattern_count >= 1:
            print(f'{file} has word "Treasure" with total count = {pattern_count}')
            total_count += 1
        else:
            print(f"{file} doesn't contain the word {pattern} ")
    print(f'{total_count} files contains the word {pattern}.')
