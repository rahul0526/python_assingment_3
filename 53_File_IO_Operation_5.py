# Open the file in read & write mode and apply following functions
# All 13 functions mentioned in Tutorial File object table.
# 1. close() function
file_open = open('Sample.txt', 'r')
file_open.close()

# 2. fileno() function
file_open = open('Sample.txt', 'r')
print(file_open.fileno())
file_open.close()

# 3. flush() function
file_open = open('Sample.txt', 'r')
file_open.flush()
file_open.close()

# 4. isatty() function
file_open = open('Sample.txt', 'r')
print(file_open.isatty())
file_open.close()

# 5. read() function
file_open = open('Sample.txt', 'r')
print(file_open.read(30))
file_open.close()

# 6. readline() function
file_open = open('Sample.txt', 'r')
print(file_open.readline())
file_open.close()

# 7. readlines() function
file_open = open('Sample.txt', 'r')
print(file_open.readlines(20))
file_open.close()

# 8. readable() function
file_open = open('Sample.txt', 'r')
print(file_open.readable())
file_open.close()

# 9. seek() function
file_open = open('Sample.txt', 'r')
print(file_open.read(6))
file_open.seek(12)
print(file_open.read(6))
file_open.close()

# 10. tell() function
file_open = open('Sample.txt', 'r')
file_open.readline()
print(file_open.tell())
file_open.close()

# 11. truncate() function
file_open = open('Sample.txt', 'r+')
print(file_open.readline())
file_open.close()
file_open = open('Sample.txt', 'w+')
file_open.truncate(600)
file_open.close()
file_open = open('Sample.txt', 'r+')
print(file_open.readline())
file_open.close()

# 12. write() function
file_open = open('Sample.txt', 'w')
to_write = 'Hello, this is the line'
file_open.write(to_write)
file_open = open('Sample.txt', 'r')
file_open.read()
file_open.close()

# 13. writelines() function
file_open = open('Sample.txt', 'w')
to_write = '''Hello, this is the line
hi'''
file_open.writelines(to_write)
file_open = open('Sample.txt', 'r')
file_open.read()
file_open.close()
