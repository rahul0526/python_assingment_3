import math
# a) Addition/subtraction/multiplication and division of two numbers (Note: Create separate function for each operation)


def add(no1, no2):
    print(f"{no1} + {no2} = ", no1 + no2)


def subtract(no1, no2):
    print(f"{no1} - {no2} = ", no1 - no2)


def multiply(no1, no2):
    print(f"{no1} * {no2} = ", no1 * no2)


def divide(no1, no2):
    print(f"{no1} / {no2} = ", no1 / no2)


add(1, 4)
subtract(90, 65)
multiply(43, 67)
divide(366, 12)

# b) Find square root of a given number. (Use keyword arguments in your function)


def sqrt(n):
    print(f'Square root of {n} = ', math.sqrt(n))


sqrt(n=36)


# c) Create a list of sub strings from a given string, such that sub strings are created with given character.


def substring(stri, delim):
    substrin = stri.split(delim)
    print("Sub-String = ", substrin)


ip_string = "Pack: My: Box: With: Good: Food"
delimiter = ":"
substring(ip_string, delimiter)
