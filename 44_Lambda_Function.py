# Write a program with lambda function to perform following.
# a) Perform all the operations of basic calculator (Add, Sub, Multiply, Divide, Modulus, Floor division)


addition = lambda no1, no2: no1 + no2
subtraction = lambda no1, no2: no1 - no2
multiplication = lambda no1, no2: no1 * no2
division = lambda no1, no2: no1 / no2
modulus = lambda no1, no2: no1 % no2
floor_division = lambda no1, no2: no1 // no2

print("Addition : ", addition(55, 44))
print("Subtraction : ", subtraction(55, 44))
print("Multiplication : ", multiplication(55, 44))
print("Division : ", division(55, 44))
print("Modulus : ", modulus(55, 44))
print("Floor Division : ", floor_division(55, 44))

