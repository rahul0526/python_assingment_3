import math


# functions to add 2 numbers
def add_num(no1, no2):
    return no1+no2


# function to find diff of 2 numbers
def diff_num(no1, no2):
    return no1-no2


# function to multiply 2 numbers
def mult_num(no1, no2):
    return no1*no2


# all maths operations ( sqrt, div, floor div, modulus, primenumber)
def div(no1, no2):
    return no1/no2


def sqrt(no1):
    return math.sqrt(no1)


def floor_div(no1, no2):
    return no1//no2


def modulus(no1, no2):
    return no1%no2


def primenumber(no1):
    for divisor in range(2, no1):
        if no1 % divisor == 0:
            return 'Number is not Prime'

    else:
        return 'Number is Prime Number'
# Fibonacci series
def fibonacci(limit):
    first_number = 0
    second_number = 1
    count = 1
    if limit == 1:
        print(first_number)
    else:
        while count <= limit:
            third_number = first_number + second_number
            print(first_number)
            first_number = second_number
            second_number = third_number
            count += 1
