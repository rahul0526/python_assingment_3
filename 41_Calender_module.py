import calendar

# Print the 2016 calendar with space between months as 10 characters.
print('Calender of 2016 is ', calendar.calendar(2016, c=10))

# How many leap days between the years 1980 to 2025.
print("Leap days between the years 1980 to 2025 are", calendar.leapdays(1980, 2025), "days")

# Check given year is leap year or not.
InputYear = int(input("enter the year to see if leap or not"))
if calendar.isleap(InputYear):
    print("year", InputYear, "is a leap year")
else:
    print("year", InputYear, "is not a leap year")

# print calendar of any specified month of the year 2016.
InputMonth = int(input('Enter Month To Get Calender of Year 2016 (e.g. - 01 for Jan, 04 for Apr, etc ) : '))
c = calendar.TextCalendar(calendar.MONDAY)
cal = c.formatmonth(2016, InputMonth)
print(cal)
