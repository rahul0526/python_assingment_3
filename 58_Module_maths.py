# Write a new program in file "maths.py" such that you import functions of file "calc.py" to your new program
import calc
print(calc.add_num(45, 56))
print(calc.diff_num(56, 43))
print(calc.mult_num(23, 34))
print(calc.div(68, 24))
print(calc.sqrt(144))
print(calc.modulus(43, 89))
print(calc.floor_div(34, 10))
print(calc.primenumber(67))
print(calc.fibonacci(8))

# Use From <module> import <function> statement to import only few function  from calc module
from calc import add_num
from calc import mult_num
from calc import sqrt
from calc import primenumber
print(add_num(67, 78))
print(mult_num(34, 56))
print(sqrt(456))
print(primenumber(89))


